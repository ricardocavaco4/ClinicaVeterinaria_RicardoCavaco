﻿namespace ClinicaVeterinaria_RicardoCavaco.ViewModels
{

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using ClinicaVeterinaria_RicardoCavaco.Models;

    public class UserAlertViewModel
    {

        public List<Alert> AlertModel { get; set; }

        public IndexViewModel IndexViewModel { get; set; }

    }
}