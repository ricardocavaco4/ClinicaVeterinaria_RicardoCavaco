﻿using ClinicaVeterinaria_RicardoCavaco.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using System.Web.Services.Description;

[assembly: OwinStartupAttribute(typeof(ClinicaVeterinaria_RicardoCavaco.Startup))]
namespace ClinicaVeterinaria_RicardoCavaco
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            
        }
    }
}
