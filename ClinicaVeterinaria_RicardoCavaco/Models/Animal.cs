﻿namespace ClinicaVeterinaria_RicardoCavaco.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public enum Species
    {
        Canino,
        Felino,
        Réptil,
        Aracnídeo,
        Aquático,
        Outro
    }

    public enum Genders
    {
        Masculino,
        Feminino,
        Outro
    }


    public class Animal
    {
        
        [Key]
        public int AnimalID { get; set; }

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o animal!")]
        [StringLength(250, ErrorMessage = "O campo {0} tem entre {2} a {1} caracteres!", MinimumLength = 3)]
        public string Name { get; set; }


        [Display(Name = "Idade em anos")]
        [Required(ErrorMessage = "Tem que inserir uma {0} para o animal!")]
        [DisplayFormat(DataFormatString = "{0:0.0}", ApplyFormatInEditMode = false)]
        public float Age { get; set; }

        [Display(Name = "Espécie")]
        [Required(ErrorMessage = "Tem que inserir uma {0} para o animal!")]
        public Species Specie { get; set; }

        [Display(Name = "Género")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o animal!")]
        public Genders Gender { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        [Display(Name = "Dono")]
        public int ClientID { get; set; }

        public virtual Client Client { get; set; }

        public virtual ICollection<Appointment> Appointments { get; set; }


    }

}