﻿namespace ClinicaVeterinaria_RicardoCavaco.Models
{

    using System;
    using System.ComponentModel.DataAnnotations;

    public enum MessageType
    {

        Aviso_Prévio = 0,
        Desmarcação = 1,
        Aviso_Geral = 2

    }

    public class Alert
    {

        [Key]
        public int AlertID { get; set; }
      
        [Display(Name = "Conteúdo")]
        [Editable(false)]
        public string Description { get; set; }

        [Display(Name = "Tipo de Alerta")]
        public string AlertType { get; set; }

        [Display(Name = "Data de Envio")]
        [DataType(DataType.Date)]
        [Editable(false)]
        public DateTime DateOfAlert { get; set; }

        [Display(Name = "Cliente")]
        public int? ClientID { get; set; }

        [Display(Name = "Veterinário")]
        public int? VetID { get; set; }

        public virtual Client Client { get; set; }

        public virtual Vet Vet { get; set; }

    }

}