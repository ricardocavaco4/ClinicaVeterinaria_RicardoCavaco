﻿namespace ClinicaVeterinaria_RicardoCavaco.Models
{

    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public enum Hours
    {
        _09s30,
        _10s00,
        _10s30,
        _11s00,
        _11s30,
        _12s00,
        _14s00,
        _14s30,
        _15s00,
        _15s30,
        _16s00,
        _16s30,
        _17s00,
        _17s30,
        _18s00,

    }

    public enum State
    {

        Espera,//Quando o utilizador marca uma consulta.
        Confirmada,//Quando o admin confirma a consulta.
        Cancelada,//Quando é cancelada por motivos.
        Efetuada//Consulta completa com sucesso.

    }

    public class Appointment
    {

        [NotMapped]
        private string hours;

        [Key]
        public int AppointmentID { get; set; }

        [Display(Name = "Data da Consulta")]
        [DataType(DataType.Date)]
        public DateTime DateAppointment { get; set; }

        [Display(Name = "Hora da Consulta")]
        [DataType(DataType.Time)]
        public string HourAppointment
        {
            get { return hours; }
            set
            {

                if(value != null)
                {
                    hours = value.Replace('_', ' ').Replace('s', ':');
                }
                
            }
        }

        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        [Display(Name = "Animal")]
        public int AnimalID { get; set; }

        [Required(ErrorMessage = "O campo {0} é obrigatório!")]
        [Display(Name = "Veterinário")]
        public int VetID { get; set; }

        public virtual Animal Animal { get; set; }

        public virtual Vet Vet { get; set; }

        [Display(Name ="Estado")]
        public State State { get; set; }

    }

}