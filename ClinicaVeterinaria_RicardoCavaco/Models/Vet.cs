﻿namespace ClinicaVeterinaria_RicardoCavaco.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public enum StatusVet
    {

        Trabalhando = 0,
        Férias = 1,
        Suspênsão = 2,
        Demissão = 3

    }

    public class Vet
    {

        [Key]
        public int VetID { get; set; }

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o veterinário!")]
        [StringLength(250, ErrorMessage = "O campo {0} tem entre {2} a {1} caracteres!", MinimumLength = 3)]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name="E-Mail")]
        public string Email { get; set; }

        [Display(Name = "Estado")]
        public StatusVet Status { get; set; }

        [Display(Name = "Salário")]
        [Required(ErrorMessage = "Tem que inserir um {0}!")]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = false)]
        public decimal Wage { get; set; }

        public virtual ICollection<Appointment> Appointments { get; set; }

        public virtual ICollection<Alert> Alerts { get; set; }

    }

}