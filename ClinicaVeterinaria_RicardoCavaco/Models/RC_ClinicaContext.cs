﻿namespace ClinicaVeterinaria_RicardoCavaco.Models
{

    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public class RC_ClinicaContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public RC_ClinicaContext() : base("name=RC_ClinicaContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

        }

        public System.Data.Entity.DbSet<ClinicaVeterinaria_RicardoCavaco.Models.Client> Clients { get; set; }

        public System.Data.Entity.DbSet<ClinicaVeterinaria_RicardoCavaco.Models.Animal> Animals { get; set; }

        public System.Data.Entity.DbSet<ClinicaVeterinaria_RicardoCavaco.Models.Vet> Vets { get; set; }

        public System.Data.Entity.DbSet<ClinicaVeterinaria_RicardoCavaco.Models.Appointment> Appointments { get; set; }

        public System.Data.Entity.DbSet<ClinicaVeterinaria_RicardoCavaco.Models.Alert> Alerts { get; set; }

    }
}
