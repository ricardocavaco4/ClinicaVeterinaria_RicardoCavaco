﻿namespace ClinicaVeterinaria_RicardoCavaco.Models
{

    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    public class Client
    {

        [Key]
        public int ClientID { get; set; }

        [Display(Name = "Nome")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o cliente!")]
        [StringLength(250, ErrorMessage = "O campo {0} tem entre {2} a {1} caracteres!", MinimumLength = 3)]
        public string Name { get; set; }


        [Display(Name = "Nº de Telefone")]
        [Required(ErrorMessage = "Tem que inserir um {0} para o cliente!")]
        [StringLength(100, ErrorMessage = "O campo {0} tem entre {2} a {1} caracteres!", MinimumLength = 9)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Display(Name = "Morada")]
        [Required(ErrorMessage = "Tem que inserir uma {0} para o cliente!")]
        [StringLength(250, ErrorMessage = "O campo {0} tem entre {2} a {1} caracteres!", MinimumLength = 3)]
        public string Address { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage ="É obrigatório inserir um e-mail válido!")]
        public string Email { get; set; }

        public virtual ICollection<Animal> Animals { get; set; }

        public virtual ICollection<Alert> Alerts { get; set; }
        


    }
}