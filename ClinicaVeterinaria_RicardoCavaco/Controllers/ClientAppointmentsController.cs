﻿namespace ClinicaVeterinaria_RicardoCavaco.Controllers
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;
    using ClinicaVeterinaria_RicardoCavaco.Models;
    using Helpers;
    using Microsoft.AspNet.Identity;

    [Authorize]
    public class ClientAppointmentsController : Controller
    {
        private RC_ClinicaContext db = new RC_ClinicaContext();

        // GET: ClientAppointments
        public ActionResult Index()
        {

            string userName = System.Web.HttpContext.Current.User.Identity.GetUserName();

            if (string.IsNullOrWhiteSpace(userName))
            {
                return null;
            }

            var query = (from c in db.Clients
                         where userName == c.Name
                         select c).Single();

            var appointments = DropDownListHelper.GetAppoitntmentsByClientID(query.ClientID);

            return View(appointments.ToList());
        }

        // GET: ClientAppointments/Create
        public ActionResult Create()
        {

            string userName = System.Web.HttpContext.Current.User.Identity.GetUserName();

            if (string.IsNullOrWhiteSpace(userName))
            {
                return null;
            }

            int query = (from c in db.Clients

                         where userName == c.Name
                         select c.ClientID).Single();

            if (query < 1)
            {
                return null;
            }

            ViewBag.HourAppointment = new SelectList(DropDownListHelper.GetHours());
            ViewBag.AnimalID = new SelectList(DropDownListHelper.GetAnimalNameListByClientID(query), "AnimalID", "Name");
            ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name");

            return View();

        }

        // POST: ClientAppointments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AppointmentID,DateAppointment,HourAppointment,AnimalID,VetID,State")] Appointment appointment)
        {

            string userName = System.Web.HttpContext.Current.User.Identity.GetUserName();

            int query = (from c in db.Clients

                         where userName == c.Name
                         select c.ClientID).Single();

            if (appointment.AnimalID == 0 && appointment.VetID == 0)
            {

                ViewBag.AnimalID = new SelectList(DropDownListHelper.GetAnimalNameListByClientID(query), "AnimalID", "Name", appointment.AnimalID);
                ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name", appointment.VetID);

                ViewBag.Error = "Deve selecionar um animal e um veterinário!";

                return View(appointment);

            }
            else if(appointment.AnimalID == 0 && appointment.VetID != 0)
            {
                ViewBag.AnimalID = new SelectList(DropDownListHelper.GetAnimalNameListByClientID(query), "AnimalID", "Name", appointment.AnimalID);
                ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name", appointment.VetID);

                ViewBag.Error = "Deve selecionar um animal!";

                return View(appointment);
            }
            else if (appointment.AnimalID != 0 && appointment.VetID == 0)
            {
                ViewBag.AnimalID = new SelectList(DropDownListHelper.GetAnimalNameListByClientID(query), "AnimalID", "Name", appointment.AnimalID);
                ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name", appointment.VetID);

                ViewBag.Error = "Deve selecionar um veterinário!";

                return View(appointment);
            }

            if (string.IsNullOrWhiteSpace(userName))
            {
                return null;
            }

            if (query < 1)
            {
                return null;
            }

            if (DateTime.Now.Date >= appointment.DateAppointment)
            {
                ViewBag.Error = "A data de marcação tem de ser maior que a data atual!";
                ViewBag.AnimalID = new SelectList(DropDownListHelper.GetAnimalNameListByClientID(query), "AnimalID", "Name", appointment.AnimalID);
                ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name", appointment.VetID);
                ViewBag.HourAppointment = new SelectList(DropDownListHelper.GetHours());
                return View(appointment);
            }


            if (ModelState.IsValid)
            {
                appointment.State = State.Espera;
                db.Appointments.Add(appointment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            

            ViewBag.HourAppointment = new SelectList(DropDownListHelper.GetHours());
            ViewBag.AnimalID = new SelectList(DropDownListHelper.GetAnimalNameListByClientID(query), "AnimalID", "Name", appointment.AnimalID);
            ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name", appointment.VetID);
            return View(appointment);
        }

        // GET: ClientAppointments/Delete/5
        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = db.Appointments.Find(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }
            return View(appointment);
        }

        // POST: ClientAppointments/Delete/5
        [HttpPost, ActionName("Modify")]
        [ValidateAntiForgeryToken]
        public ActionResult ModifyConfirmed(int id)
        {

            Appointment appointment = db.Appointments.Find(id);

            if (ModelState.IsValid)
            {

                appointment.State = State.Cancelada;
                db.Entry(appointment).State = EntityState.Modified;               
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }

            return RedirectToAction("Modify");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
