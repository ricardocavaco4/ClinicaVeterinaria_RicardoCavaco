﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicaVeterinaria_RicardoCavaco.Helpers;
using ClinicaVeterinaria_RicardoCavaco.Models;

namespace ClinicaVeterinaria_RicardoCavaco.Controllers
{
    [Authorize(Users = "ricardocavaco4@gmail.com")]
    public class AppointmentsController : Controller
    {

        private RC_ClinicaContext db = new RC_ClinicaContext();

        // GET: Appointments
        public ActionResult Index()
        {
            var appointments = db.Appointments.Include(a => a.Animal).Include(a => a.Vet);
            return View(appointments.ToList());
        }

        // GET: Appointments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = db.Appointments.Find(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }
            return View(appointment);
        }

        // GET: Appointments/Create
        public ActionResult Create()
        {
            ViewBag.AnimalID = new SelectList(db.Animals, "AnimalID", "Name");
            ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name");
            ViewBag.HourAppointment = new SelectList(DropDownListHelper.GetHours());

            return View();

        }

        // POST: Appointments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AppointmentID,DateAppointment,HourAppointment,AnimalID,VetID")] Appointment appointment)
        {


            if(DateTime.Now.Date >= appointment.DateAppointment)
            {
                ViewBag.Error = "A data de marcação tem de ser maior que a data atual!";
                ViewBag.AnimalID = new SelectList(db.Animals, "AnimalID", "Name", appointment.AnimalID);
                ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name", appointment.VetID);
                ViewBag.HourAppointment = new SelectList(DropDownListHelper.GetHours());
                return View(appointment);
            }

            if (ModelState.IsValid)
            {

                db.Appointments.Add(appointment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AnimalID = new SelectList(db.Animals, "AnimalID", "Name", appointment.AnimalID);
            ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name", appointment.VetID);
            ViewBag.HourAppointment = new SelectList(DropDownListHelper.GetHours());
            return View(appointment);
        }

        // GET: Appointments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = db.Appointments.Find(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }
            ViewBag.AnimalID = new SelectList(db.Animals, "AnimalID", "Name", appointment.AnimalID);
            ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name", appointment.VetID);
            ViewBag.HourAppointment = new SelectList(DropDownListHelper.GetHours());
            return View(appointment);
        }

        // POST: Appointments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AppointmentID,DateAppointment,HourAppointment,AnimalID,VetID")] Appointment appointment)
        {

            if (DateTime.Now.Date >= appointment.DateAppointment)
            {
                ViewBag.Error = "A data de marcação tem de ser maior que a data atual!";
                ViewBag.AnimalID = new SelectList(db.Animals, "AnimalID", "Name", appointment.AnimalID);
                ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name", appointment.VetID);
                ViewBag.HourAppointment = new SelectList(DropDownListHelper.GetHours());
                return View(appointment);
            }

            if (ModelState.IsValid)
            {

                db.Entry(appointment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AnimalID = new SelectList(db.Animals, "AnimalID", "Name", appointment.AnimalID);
            ViewBag.VetID = new SelectList(DropDownListHelper.GetVetNameList(), "VetID", "Name", appointment.VetID);
            ViewBag.HourAppointment = new SelectList(DropDownListHelper.GetHours());

            return View(appointment);

        }

        // GET: Appointments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = db.Appointments.Find(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }
            return View(appointment);
        }

        // POST: Appointments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Appointment appointment = db.Appointments.Find(id);
            db.Appointments.Remove(appointment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: ClientAppointments/Delete/5
        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Appointment appointment = db.Appointments.Find(id);
            if (appointment == null)
            {
                return HttpNotFound();
            }
            return View(appointment);
        }

        // POST: ClientAppointments/Delete/5
        [HttpPost, ActionName("Modify")]
        [ValidateAntiForgeryToken]
        public ActionResult ModifyConfirmed(int id)
        {

            Appointment appointment = db.Appointments.Find(id);

            if (ModelState.IsValid)
            {

                appointment.State = State.Confirmada;
                db.Entry(appointment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Modify");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }

}
