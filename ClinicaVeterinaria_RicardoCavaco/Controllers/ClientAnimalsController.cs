﻿namespace ClinicaVeterinaria_RicardoCavaco.Controllers
{

    using System.Data;
    using System.Linq;
    using System.Web.Mvc;
    using ClinicaVeterinaria_RicardoCavaco.Helpers;
    using ClinicaVeterinaria_RicardoCavaco.Models;
    using Microsoft.AspNet.Identity;

    public class ClientAnimalsController : Controller
    {
        private RC_ClinicaContext db = new RC_ClinicaContext();

        // GET: ClientAnimals
        public ActionResult Index()
        {

            string userName = System.Web.HttpContext.Current.User.Identity.GetUserName();

            if (string.IsNullOrWhiteSpace(userName))
            {
                return null;
            }

            var query = (from c in db.Clients
                         where userName == c.Name
                         select c).Single();

            //var animals = db.Animals.Include(a => a.Client);
            var animals = DropDownListHelper.GetAnimalsByClientID(query.ClientID);
            return View(animals.ToList());

        }

        // GET: ClientAnimals/Create
        public ActionResult Create()
        {
            
            return View();

        }

        // POST: ClientAnimals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AnimalID,Name,Age,Specie,Gender,ClientID")] Animal animal)
        {

            string userName = System.Web.HttpContext.Current.User.Identity.GetUserName();

            if (string.IsNullOrWhiteSpace(userName))
            {
                return null;
            }

            var query = (from c in db.Clients
                         where userName == c.Name
                         select c).Single();

            if (query == null)
            {
                return null;
            }

            if (ModelState.IsValid)
            {
                animal.ClientID = query.ClientID;

                db.Animals.Add(animal);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           
            return View(animal);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }

}
