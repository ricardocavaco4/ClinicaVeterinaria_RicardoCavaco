﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicaVeterinaria_RicardoCavaco.Helpers;
using ClinicaVeterinaria_RicardoCavaco.Models;

namespace ClinicaVeterinaria_RicardoCavaco.Controllers
{
    [Authorize(Users = "ricardocavaco4@gmail.com")]
    public class AlertsController : Controller
    {
        private RC_ClinicaContext db = new RC_ClinicaContext();

        // GET: Alerts
        public ActionResult Index()
        {
            var alerts = db.Alerts.Include(a => a.Client).Include(a => a.Vet);
            return View(alerts.ToList());
        }

        /// <summary>
        /// Envia alertas para os clientes.
        /// </summary>
        /// <returns></returns>
        public ActionResult Generate()
        {

            List<Client> clients = (from c in db.Clients select c).ToList();

            if (clients.Count != 0)
            {

                //Escolhe um dia ao random:
                Random r = new Random();
                int days = r.Next(1, 8);

                foreach (Client c in clients)
                {

                    if (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
                    {

                        CreateGeneralAlerts(c, days);

                    }

                    

                    var consultas = (from co in db.Appointments where co.Animal.ClientID == c.ClientID select co).ToList();

                    if (consultas.Count != 0)
                    {

                        foreach (var ap in consultas)
                        {

                            if (ap.DateAppointment.ToShortDateString() == DateTime.Now.AddDays(1).ToShortDateString())
                            {

                                CreateWarningAlerts(c);

                            }

                        }

                    }

                    var veterinarios = (from ve in db.Vets join ap in db.Appointments on ve.VetID equals ap.VetID join an in db.Animals on ap.AnimalID equals an.AnimalID where ve.Status != StatusVet.Trabalhando && an.ClientID == c.ClientID select ve).ToList();

                    if (veterinarios.Count != 0)
                    {

                        foreach (var vet in veterinarios)
                        {

                            CreateVetAlerts(c, vet);

                        }

                    }

                }

            }

            var alerts = db.Alerts.Include(a => a.Client).Include(a => a.Vet);
            return View("Index", alerts.ToList());

        }

        /// <summary>
        /// Envia alertas para todos os utilizadores consoante um dia à escolha.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="days"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void CreateGeneralAlerts(Client client, int days)
        {

            if (ModelState.IsValid)
            {

                try
                {

                    Alert alert = new Alert();

                    alert.Description = "";

                    alert.AlertType = MessageType.Aviso_Geral.ToString().Replace('_', ' ');
                    alert.Client = client;
                    alert.Vet = null;
                    alert.DateOfAlert = DateTime.Now;

                    string date = DateTime.Now.Date.AddDays(days).ToString("dd-MM-yyyy");

                    alert.Description = $"Caro/a {alert.Client.Name}, viemos por este meio informar que no próximo dia {DateTime.Now.Date.AddDays(days).ToString("dd-MM-yyyy")}, a nossa clínica irá-se encontrar fechada a qualquer tipo de cliente, pedimos desculpa pelo incómodo!";

                    db.Alerts.Add(alert);
                    db.SaveChanges();

                    List<Appointment> app = (from ap in db.Appointments where ap.DateAppointment.ToShortDateString() == date && client.ClientID == alert.ClientID select ap).ToList();

                    foreach (var ap in app)
                    {
                        db.Entry(ap).State = EntityState.Modified;
                        db.SaveChanges();
                    }



                }
                catch (Exception e)
                {
                    string m = e.Message;
                }


            }

        }

        /// <summary>
        /// Envia alertas de aviso prévio para os utilizadores com consulta.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="days"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void CreateWarningAlerts(Client client)
        {

            if (ModelState.IsValid)
            {

                try
                {

                    Alert alert = new Alert();

                    alert.Description = "";

                    alert.AlertType = MessageType.Aviso_Prévio.ToString().Replace('_', ' ');
                    alert.Client = client;
                    alert.Vet = null;
                    alert.DateOfAlert = DateTime.Now;

                    alert.Description = $"Caro/a {alert.Client.Name}, viemos por este meio relembrar, que, amanhã, no dia {DateTime.Now.Date.AddDays(1).ToString("dd-MM-yyyy")} foi agendada uma consulta, por favor não falte!";

                    db.Alerts.Add(alert);
                    db.SaveChanges();


                }
                catch (Exception e)
                {
                    string m = e.Message;
                }


            }

        }

        /// <summary>
        /// Envia alertas de aviso de desmarcação de consulta devido à mudança de status do veterinário.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="days"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void CreateVetAlerts(Client client, Vet vet)
        {

            if (ModelState.IsValid)
            {

                try
                {

                    Alert alert = new Alert();

                    alert.Description = "";

                    alert.AlertType = MessageType.Desmarcação.ToString();
                    alert.Client = client;
                    alert.Vet = vet;
                    alert.DateOfAlert = DateTime.Now;

                    List<Appointment> app = (from ap in db.Appointments where ap.VetID == alert.Vet.VetID && client.ClientID == alert.ClientID select ap).ToList();

                    foreach(var ap in app)
                    {
                        db.Entry(ap).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    alert.Description = $"Caro/a {alert.Client.Name}, viemos por este meio informar que a sua consulta agendada com o/a Doutor/a {alert.Vet.Name} foi desmarcada devido a motivos pessoais com este/a profissional, pedimos imensas desculpas, iremos contacta-lo/a o mais breve possivél!";

                    db.Alerts.Add(alert);
                    db.SaveChanges();

                }
                catch (Exception e)
                {
                    string m = e.Message;
                }


            }

        }

        // GET: Alerts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alert alert = db.Alerts.Find(id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            return View(alert);
        }

        // GET: Alerts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alert alert = db.Alerts.Find(id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            return View(alert);
        }

        // POST: Alerts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Alert alert = db.Alerts.Find(id);
            db.Alerts.Remove(alert);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
