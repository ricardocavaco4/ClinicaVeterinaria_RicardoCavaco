﻿namespace ClinicaVeterinaria_RicardoCavaco.Helpers
{
    using ClinicaVeterinaria_RicardoCavaco.Models;
    using System.Collections.Generic;
    using System.Web;
    using System.Linq;
    using Microsoft.AspNet.Identity;

    public static class AlertHelper
    {

        private static RC_ClinicaContext db = new RC_ClinicaContext();
        private static ApplicationDbContext da = new ApplicationDbContext();

        public static List<Alert> GetAlertsByUserName()
        {
            List<Alert> listAlerts = new List<Alert>();

            string userName = HttpContext.Current.User.Identity.GetUserName();

            if (string.IsNullOrWhiteSpace(userName))
            {
                return null;
            }

            var query = (from a in db.Alerts
                         join c in db.Clients 
                         on a.ClientID equals c.ClientID
                         where userName == c.Name select a).ToList();

            if (query.Count == 0)
            {
                return null;
            }

            return query;

        }

    }

}