﻿using ClinicaVeterinaria_RicardoCavaco.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ClinicaVeterinaria_RicardoCavaco.Helpers
{
    public static class DropDownListHelper
    {

        private static RC_ClinicaContext db = new RC_ClinicaContext();

        /// <summary>
        /// Lista de todos os clientes.
        /// </summary>
        /// <returns></returns>
        public static List<Client> GetClientNameList()
        {

            List<Client> Clients = db.Clients.ToList();

            Clients.Add(new Client { ClientID=0, Name = "[Selecione um cliente]" });

            return Clients.OrderBy(t => t.Name).ToList();

        }

        /// <summary>
        /// Lista de todos os veterinários a trabalhar.
        /// </summary>
        /// <returns></returns>
        public static List<Vet> GetVetNameList()
        {

            List<Vet> Vets = (from v in db.Vets where v.Status == StatusVet.Trabalhando select v).ToList();

            Vets.Add(new Vet { VetID = 0, Name = "[Selecione um veterinário]" });

            return Vets.OrderBy(x => x.VetID).ToList();

        }

        /// <summary>
        /// Lista todos os animais.
        /// </summary>
        /// <returns></returns>
        public static List<Animal> GetAnimalNameList()
        {

            List<Animal> Animals = (from a in db.Animals select a).ToList();

            Animals.Add(new Animal { AnimalID = 0, Name = "[Selecione um animal]" });

            return Animals.OrderBy(x=>x.AnimalID).ToList();

        }

        /// <summary>
        /// Lista todos os animais do cliente atual.
        /// </summary>
        /// <param name="idCliente">Id do cliente atual</param>
        /// <returns></returns>
        public static List<Animal> GetAnimalNameListByClientID(int idCliente)
        {

            List<Animal> Animals = (from a in db.Animals where a.ClientID == idCliente select a).ToList();

            Animals.Add(new Animal { AnimalID = 0, Name = "[Selecione um animal]" });

            return Animals.OrderBy(x => x.AnimalID).ToList();

        }

        /// <summary>
        /// Converte o enum Hours para uma lista.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<string> GetHours()
        {
            List<string> hrs = new List<string>();

            var valores = Enum.GetNames(typeof(Hours));

            foreach(var x in valores)
            {
                hrs.Add(x.Replace('_',' ').Replace('s',':'));
            }

            return hrs;

        }

        /// <summary>
        /// Converte o enum MessageTypes para uma lista.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetAlertTypes()
        {
            List<string> types = new List<string>();

            var valores = Enum.GetNames(typeof(MessageType));

            foreach (var x in valores)
            {
                types.Add(x.Replace('_', ' '));
            }

            return types;
            
        }

        /// <summary>
        /// Recebe todas os animais de um cliente.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<Animal> GetAnimalsByClientID(int id)
        {

            List<Animal> Animals = (from a in db.Animals where a.ClientID == id select a).ToList();

            return Animals.OrderBy(x => x.AnimalID).ToList();

        }

        /// <summary>
        /// Recebe todas as consultas de um cliente.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static List<Appointment> GetAppoitntmentsByClientID(int id)
        {

            List<Appointment> Appointments = (from c in db.Appointments join d in db.Animals on c.AnimalID equals d.AnimalID where d.ClientID == id select c).ToList();

            return Appointments.OrderBy(x => x.AppointmentID).ToList();

        }

    }
}